﻿namespace BanksySan.Learning.Memory.TestCases
{
    using System;
    using Presentation;

    class Generations : ITestCase
    {
        private readonly IPresenter _presenter;

        public Generations(IPresenter presenter)
        {
            _presenter = presenter;
        }

        public void Dispose() { }

        public void Start()
        {
            _presenter.NewTest(".NET Memory Generations");
            var maxGeneration = GC.MaxGeneration;

            _presenter.WriteLine("We will create a new object `o`.");
            var o = new object();
            _presenter.WriteLine(
                $"Now we will call the GC several times and see the object move up the generations.  There are {maxGeneration + 1} generations.");
            for (var i = 0; i < maxGeneration + 2; i++)
            {
                DoCollection(o);
            }

            GC.TryStartNoGCRegion(10000000);
            _presenter.WriteLine();
            _presenter.WriteLine(
                "Now we will see that the GC will collect the object when there are no `string` references.");
            
            var weakReference = new WeakReference(o);
            _presenter.WriteLine($"Is the object collected? {(weakReference.IsAlive ? "No" : "Yes")}");
            GC.EndNoGCRegion();
            DoCollection(o);
            
            _presenter.WriteLine($"Is the object collected? {(weakReference.IsAlive ? "No" : "Yes")}");
        }

        private void DoCollection(object o)
        {
            _presenter.WriteLine($"The object is in generation {GC.GetGeneration(o)}.");
            _presenter.WriteLine("Now we'll call the garbage collection.");
            GC.Collect();
        }

        public string Description => "See how the GC moves objects through the generations.";
    }
}