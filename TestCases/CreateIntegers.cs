﻿namespace BanksySan.Learning.Memory.TestCases
{
    using System;
    using System.Runtime.CompilerServices;
    using Presentation;

    public class LargeObjectHeap : ITestCase
    {
        private readonly IPresenter _presenter;

        public LargeObjectHeap(IPresenter presenter)
        {
            _presenter = presenter;
        }

        public void Dispose() { }

        [MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
        public void Start()
        {
            var random = new Random(0);
            _presenter.PromptForSnapshot();
            const int NUMBER_TO_CREATE = 100000000;
            var ints = new int[NUMBER_TO_CREATE];
            _presenter.WriteLine($"We now have a very large integer array of {NUMBER_TO_CREATE:N} elements.  As an integer is a value type, all the elements are defaulted to zero, i.e. default(int) == 0.");
            _presenter.PromptForSnapshot();
            _presenter.WriteLine("Now we'll put non-zero values in these array elements.");

            for (var i = 0; i < NUMBER_TO_CREATE; i++)
            {
                ints[i] = random.Next();
            }
            _presenter.WriteLine("Replaced all the array elements with random numbers");
            _presenter.PromptForSnapshot();
            var generation = GC.GetGeneration(ints);
            _presenter.WriteLine($"The array is in gen-{generation}.");

        }

        public string Description => "Create a large object.  You will see this is put in the LOH.";
    }
}