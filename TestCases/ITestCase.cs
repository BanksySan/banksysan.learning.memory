﻿namespace BanksySan.Learning.Memory.TestCases
{
    using System;

    public interface ITestCase : IDisposable
    {
        void Start();
        string Description { get; }
    }
}