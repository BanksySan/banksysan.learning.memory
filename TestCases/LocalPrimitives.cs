﻿namespace BanksySan.Learning.Memory.TestCases
{
    using System;
    using System.Runtime.CompilerServices;
    using Presentation;

    class LocalPrimitives : ITestCase
    {
        private static readonly Random RANDOM = new Random(1);

        private readonly IPresenter _presenter;

        public LocalPrimitives(IPresenter presenter)
        {
            _presenter = presenter;
        }

        public void Dispose() { }

        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public void Start()
        {
            _presenter.NewTest("Local Primitives");
            _presenter.PromptForSnapshot();
            _presenter.WriteLine("Now we're going to create a sixty local integers (0 -> 59).");

            var i0 = RANDOM.Next();
            var i1 = RANDOM.Next();
            var i2 = RANDOM.Next();
            var i3 = RANDOM.Next();
            var i4 = RANDOM.Next();
            var i5 = RANDOM.Next();
            var i6 = RANDOM.Next();
            var i7 = RANDOM.Next();
            var i8 = RANDOM.Next();
            var i9 = RANDOM.Next();

            var i10 = RANDOM.Next();
            var i11 = RANDOM.Next();
            var i12 = RANDOM.Next();
            var i13 = RANDOM.Next();
            var i14 = RANDOM.Next();
            var i15 = RANDOM.Next();
            var i16 = RANDOM.Next();
            var i17 = RANDOM.Next();
            var i18 = RANDOM.Next();
            var i19 = RANDOM.Next();

            var i20 = RANDOM.Next();
            var i21 = RANDOM.Next();
            var i22 = RANDOM.Next();
            var i23 = RANDOM.Next();
            var i24 = RANDOM.Next();
            var i25 = RANDOM.Next();
            var i26 = RANDOM.Next();
            var i27 = RANDOM.Next();
            var i28 = RANDOM.Next();
            var i29 = RANDOM.Next();

            var i30 = RANDOM.Next();
            var i31 = RANDOM.Next();
            var i32 = RANDOM.Next();
            var i33 = RANDOM.Next();
            var i34 = RANDOM.Next();
            var i35 = RANDOM.Next();
            var i36 = RANDOM.Next();
            var i37 = RANDOM.Next();
            var i38 = RANDOM.Next();
            var i39 = RANDOM.Next();

            var i40 = RANDOM.Next();
            var i41 = RANDOM.Next();
            var i42 = RANDOM.Next();
            var i43 = RANDOM.Next();
            var i44 = RANDOM.Next();
            var i45 = RANDOM.Next();
            var i46 = RANDOM.Next();
            var i47 = RANDOM.Next();
            var i48 = RANDOM.Next();
            var i49 = RANDOM.Next();

            var i50 = RANDOM.Next();
            var i51 = RANDOM.Next();
            var i52 = RANDOM.Next();
            var i53 = RANDOM.Next();
            var i54 = RANDOM.Next();
            var i55 = RANDOM.Next();
            var i56 = RANDOM.Next();
            var i57 = RANDOM.Next();
            var i58 = RANDOM.Next();
            var i59 = RANDOM.Next();

            _presenter.PromptForSnapshot();
        }

        public string Description => "Local , will increase memory but not the heap size.";
    }
}