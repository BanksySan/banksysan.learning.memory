﻿namespace BanksySan.Learning.Memory.TestCases
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Text;
    using Presentation;

    public class BasicStrings : ITestCase
    {
        private readonly IPresenter _presenter;

        public BasicStrings(IPresenter presenter)
        {
            _presenter = presenter;
        }

        public void Dispose() { }

        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public void Start()
        {
            var stream = Stream.Null;

            const int NUMBER_OF_STRINGS = 100;

            _presenter.NewTest("Creating strings");
            _presenter.PromptForSnapshot();

            _presenter.WriteLine($"Now we are going to create a set of {NUMBER_OF_STRINGS} unique strings.");
            
            var strings = new string[NUMBER_OF_STRINGS];

            for (var i = 0; i < strings.Length; i++)
            {
                var s = Guid.NewGuid().ToString("B");
                strings[i] = s;
            }

            _presenter.PromptForSnapshot();
            foreach (var s in strings)
            {
                stream.Write(Encoding.ASCII.GetBytes(s), 0, s.Length);
            }
            _presenter.WriteLine("Now, we'll call the garbage collector");
            GC.Collect();
            _presenter.PromptForSnapshot();

            _presenter.WriteLine("All done, have a look at the snapshots.");
        }

        public string Description => "Load some strings";
    }
}