﻿namespace BanksySan.Learning.Memory
{
    using System;
    using System.Linq;
    using Presentation;
    using TestCases;

    internal class Program
    {
        private static void Main()
        {
            Console.WindowWidth = 100;
            Console.BufferWidth = Console.WindowWidth;

            Console.Title = "BanksySan.Learning.Memory";

            var gui = new ConsoleGui();

            var presenter = new Presenter(gui);

            var tests = new ITestCase[]
                        {
                            new LocalPrimitives(presenter),
                            new BasicStrings(presenter),
                            new LargeObjectHeap(presenter),
                            new Generations(presenter)
                        };

            var selectedOptionNumber = RunStartMenu(presenter, tests);

            var test = tests[selectedOptionNumber - 1];

            test.Start();

            Console.WriteLine();
            Console.WriteLine("Done...");
            Console.Read();
        }

        private static int RunStartMenu(Presenter presenter, ITestCase[] tests)
        {
            var descriptions = tests.Select(x => x.Description).ToArray();

            return presenter.ShowMenu("Select an option: ", descriptions);
        }
    }
}