namespace BanksySan.Learning.Memory.Presentation
{
    using System.Collections.Generic;

    public interface IPresenter
    {
        void NewTest(string title);
        int ShowMenu(string description, IEnumerable<string> descriptions);
        void PromptForSnapshot();
        void WriteLine(string message);
        void WriteLine();
    }
}