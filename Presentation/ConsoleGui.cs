namespace BanksySan.Learning.Memory.Presentation {
    using System;
    using System.Drawing;

    internal class ConsoleGui : IGui
    {
        public void SetResolution(Size size)
        {
            if (size.IsEmpty)
            {
                throw new InvalidOperationException("Cannot set size to zero");
            }
            
            Console.BufferHeight = size.Height;
            Console.BufferWidth = size.Width;
            
            Console.SetWindowSize(size.Width, size.Height);
        }
        
        public void Clear()
        {
            Console.Clear();
        }

        public void SetCursor(int left, int top)
        {
            Console.CursorLeft = left;
            Console.CursorTop = top;
        }

        public void Write(string value)
        {
            Console.Write(value);
        }

        public void WriteLine(string value = null)
        {
            Console.WriteLine(value);
        }

        public ConsoleKeyInfo ReadKey(bool intercept = true)
        {
            return Console.ReadKey(intercept);
        }

        public string ReadLine()
        {
            return Console.ReadLine();
        }
    }
}