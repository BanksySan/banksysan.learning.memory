﻿namespace BanksySan.Learning.Memory.Presentation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Presenter : IPresenter
    {
        private const int DIDGETS_OFFSET = 48;
        private readonly IGui _gui;

        public Presenter(IGui gui)
        {
            _gui = gui;
        }

        public void NewTest(string title)
        {
            _gui.Clear();
            _gui.SetCursor(0,0);

            WriteHeading(title, '=');
        }

        public int ShowMenu(string description, IEnumerable<string> descriptions)
        {
            _gui.Clear();
            _gui.SetCursor(0,0);
            _gui.WriteLine(description);
            _gui.WriteLine(string.Empty.PadLeft(description.Length, '='));
            _gui.WriteLine();

            var counter = 1;

            foreach (var testDescription in descriptions)
            {
                _gui.WriteLine($"{counter, 10}: {testDescription}");
                counter++;
            }

            var selectedOption = _gui.ReadLine();

            int numeric;

            while(!int.TryParse(selectedOption, out numeric) && numeric > 0 && numeric < descriptions.Count() + 1)
            {
                
            }

            return numeric;
        }

        public void PromptForSnapshot()
        {
            const string PROMPT_FOR_SNAP_SHOT = "Take a snapshot of the memory and press 'Enter' when you're done";
            _gui.WriteLine();
            HorizontalRule(PROMPT_FOR_SNAP_SHOT.Length, '-');
            _gui.WriteLine(PROMPT_FOR_SNAP_SHOT);
            HorizontalRule(PROMPT_FOR_SNAP_SHOT.Length, '-');

            var key = _gui.ReadKey();

            while (key.Key != ConsoleKey.Enter)
            {
                key = _gui.ReadKey();
            }
        }

        public void WriteLine(string message)
        {
            Console.WriteLine(message);
        }

        public void WriteLine()
        {
            Console.WriteLine();
        }

        private void WriteHeading(string title, char underlineCharacter)
        {
            _gui.WriteLine(title);
            HorizontalRule(title.Length, underlineCharacter);
        }

        private void HorizontalRule(int length, char repeatingCharacter)
        {
            _gui.WriteLine(string.Empty.PadLeft(length, repeatingCharacter));
        }
    }
}