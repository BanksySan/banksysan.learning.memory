namespace BanksySan.Learning.Memory.Presentation {
    using System;

    public interface IGui
    {
        void Clear();
        void SetCursor(int left, int top);
        void Write(string value);
        void WriteLine(string value = null);
        ConsoleKeyInfo ReadKey(bool intercept = true);
        string ReadLine();
    }
}